/* FILENAME: UMODEL.C
 * PROGRAMMER: VG4
 * PURPOSE: Model view unit handle module.
 * LAST UPDATE: 10.06.2015
 */

#include "anim.h"

/* ��� ������������� ���� */
typedef struct tagvg4UNIT_MODEL
{
  VG4_UNIT_BASE_FIELDS;

  vg4GEOM Geom[8]; /* ������ ��� ����������� */
  vg4PRIM Pr, Pl;
} vg4UNIT_MODEL;

/* ������� ������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_MODEL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitInit( vg4UNIT_MODEL *Uni, vg4ANIM *Ani )
{
  vg4VERTEX V[]= 
  {
    {{0, 0, 0}, {0, 0}, {0, 0, 1}, {1, 1, 1, 1}},
    {{1, 0, 0}, {5, 0}, {0, 0, 1}, {1, 0, 1, 1}},
    {{0, 1, 0}, {0, 5}, {0, 0, 1}, {1, 1, 0, 1}},
    {{1, 1, 0}, {5, 5}, {0, 0, 1}, {1, 1, 0, 1}},
  };
  INT I[] = {0, 1, 2, 2, 1, 3};
  BYTE txt[2][2][3] =
  {
    {{255, 255, 255}, {0, 0, 0}},
    {{0, 0, 0}, {255, 255, 255}}
  };
  vg4MATERIAL Mtl = VG4_MtlLib[0];

  //VG4_PrimCreate(&Uni->Pr, VG4_PRIM_TRIMESH, 4, 6, V, I);
  VG4_PrimCreateHeightField(&Uni->Pl, VecSet(-50, 0, -50), VecSet(0, 0, 100), VecSet(100, 0, 0), 3.50, "hfa.bmp");
  Mtl.TexId[0] = VG4_TextureLoad("GRASS.BMP");
  Mtl.TexId[1] = VG4_TextureLoad("STONE.BMP");
  Mtl.TexId[2] = VG4_TextureLoad("MASK.BMP");
  Mtl.Kd = VecSet(0.961, 0.961, 0.961);
  Mtl.Ks = VecSet(0.08, 0.08, 0.08);
  Mtl.Kp = 30;
  Uni->Pl.MtlNo = VG4_MtlAdd(&Mtl);

  VG4_PrimCreateSphere(&Uni->Pr, VecSet(0, 0, 0), 500, 30, 30);
  Uni->Pr.ProgId = VG4_ShaderLoad("AXIS");
  Mtl.TexId[0] = VG4_TextureLoad("SKY1.BMP");
  Uni->Pr.MtlNo = VG4_MtlAdd(&Mtl);

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrScale(0.3, 0.3, 0.3), MatrRotateX(-90));
  //VG4_GeomLoad(&Uni->Geom[0], "Q:\\Models\\Nissan_Pathfinder\\NISPF.G3D");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrScale(1, 1, 1), MatrRotateX(-90));
  //VG4_GeomLoad(&Uni->Geom[1], "Models\\X6\\X6.G3D");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrScale(0.01, 0.01, 0.01), MatrRotateY(180));
  //VG4_GeomLoad(&Uni->Geom[2], "MODELS\\Nissan_XTrail\\xtrail.g3d");
  
  VG4_RndPrimMatrConvert = MatrMulMatr(MatrScale(-0.007, -0.007, 0.007), MatrRotateX(-90));
  //VG4_GeomLoad(&Uni->Geom[3], "MODELS\\Ferrary_575M\\Ferrari_575M.g3d");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrScale(0.7, 0.7, 0.7), MatrRotateY(90));
  //VG4_GeomLoad(&Uni->Geom[4], "MODELS\\Avent\\avent.g3d");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrMulMatr(MatrScale(0.024, 0.024, 0.024), MatrRotateY(0)), MatrTranslate(0, -0.5, 0));
  //VG4_GeomLoad(&Uni->Geom[5], "MODELS\\Lexus_RX300\\RX300.g3d");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrMulMatr(MatrScale(0.24, 0.24, 0.24), MatrRotateX(-90)), MatrTranslate(0, 0, 0));
  //VG4_GeomLoad(&Uni->Geom[6], "MODELS\\Su27\\Su27.g3d");

  //VG4_RndPrimMatrConvert = MatrMulMatr(MatrMulMatr(MatrScale(0.24, 0.24, 0.24), MatrRotateX(0)), MatrTranslate(0, 0, 0));
  //VG4_GeomLoad(&Uni->Geom[3], "Q:\\Models\\BMW_M3_GTR\\BMW_M3_GTR.g3d");
  //VG4_GeomLoad(&Uni->Geom[2], "MODELS\\MSM\\msmunchen.g3d");

  //VG4_RndPrimMatrConvert = MatrScale(0.30, 0.30, 0.30);
  //VG4_GeomLoad(&Uni->Geom[0], "COW.G3D");
} /* End of 'VG4_AnimUnitInit' function */

/* ������� ��������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_MODEL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitClose( vg4UNIT_MODEL *Uni, vg4ANIM *Ani )
{
  INT i;

  for (i = 0; i < 8; i++)
    VG4_GeomFree(&Uni->Geom[i]);
  VG4_PrimFree(&Uni->Pr);
} /* End of 'VG4_AnimUnitClose' function */

/* ������� ���������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_MODEL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitRender( vg4UNIT_MODEL *Uni, vg4ANIM *Ani )
{
  INT i, j;

  for (i = 0; i < 1; i++)
    for (j = 0; j < 1; j++)
    {
      VG4_RndMatrWorld =
        MatrMulMatr(MatrMulMatr(MatrMulMatr(
          MatrTranslate(Ani->JX * 59, Ani->JY * 88, 0),
          MatrScale(0.1, 0.1, 0.1)),
          MatrRotateY(30 * Ani->Time + Ani->JR * 180)),
          MatrTranslate(j * 1.30, 0, i * 1.30 + 100 * Ani->JZ));
      glColor3d(i & 1, j & 1, 1 - ((i & 1) + (j & 1)) / 2);
      //VG4_GeomDraw(&Uni->Model);
    }

  for (j = -0; j <= 0; j++)
    for (i = 0; i < 8; i++)
      if (Uni->Geom[i].NumOfPrimitives != 0)
      {
        VG4_RndMatrWorld = MatrMulMatr(MatrTranslate(-4 + i * 2, 0, j * 5), MatrRotateY(30 * Ani->Time));
        VG4_GeomDraw(&Uni->Geom[i]);
      }
  //VG4_RndMatrWorld = MatrMulMatr(MatrScale(3, 3, 3), MatrRotateY(30 * Ani->Time));
  VG4_RndMatrWorld = MatrIdentity();
  VG4_RndMatrWorld = MatrMulMatr(MatrRotate(30 * Ani->Time, 1, 1, 1), MatrTranslate(0, 3, 0));
  VG4_GeomDraw(&Uni->Geom[6]);

  VG4_RndMatrWorld = MatrIdentity();
  VG4_PrimDraw(&Uni->Pl);
  //VG4_RndMatrWorld = MatrTranslate(0, 1 + fabs(sin(Ani->Time * 5) * 3), 0);
  VG4_PrimDraw(&Uni->Pr);
} /* End of 'VG4_AnimUnitRender' function */

/* ������� �������� ������� �������� "������".
 * ���������: ���.
 * ������������ ��������:
 *   (vg4UNIT *) ��������� �� ��������� ������ ��������.
 */
vg4UNIT * VG4_UnitModelCreate( VOID )
{
  vg4UNIT_MODEL *Uni;

  if ((Uni = (VOID *)VG4_AnimUnitCreate(sizeof(vg4UNIT_MODEL))) == NULL)
    return NULL;
  /* ��������� ���� */
  Uni->Init = (VOID *)VG4_AnimUnitInit;
  Uni->Close = (VOID *)VG4_AnimUnitClose;
  Uni->Render = (VOID *)VG4_AnimUnitRender;
  return (vg4UNIT *)Uni;
} /* End of 'VG4_UnitModelCreate' function */

/* END OF 'UMODEL.C' FILE */
