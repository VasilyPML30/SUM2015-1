/* FILENAME: UCONTRL.C
 * PROGRAMMER: VG4
 * PURPOSE: Control unit handle module.
 * LAST UPDATE: 10.06.2015
 */

#include <stdio.h>

#include "anim.h"

/* ��� ������������� ���� */
typedef struct tagvg4UNIT_CTRL
{
  VG4_UNIT_BASE_FIELDS;
  HFONT hFnt; /* ����� ��� ������ FPS */
  VEC
    CPos,   /* ������� ������ */
    Pos,    /* ������� ��������� */
    At;     /* ����� "��������" */
  FLT
    V,      /* �������� ��������� */
    Head, Omega; /* ���� � ������� �������� */
  vg4GEOM Helic, Axes; /* �������� � ���*/
  vg4PRIM Sph;
} vg4UNIT_CTRL;

/* ������� ������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_CTRL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitInit( vg4UNIT_CTRL *Uni, vg4ANIM *Ani )
{
  vg4PRIM pr;

  Uni->hFnt = CreateFont(30, 0, 0, 0, FW_BOLD, FALSE, FALSE,
    FALSE, RUSSIAN_CHARSET, OUT_DEFAULT_PRECIS,
    CLIP_DEFAULT_PRECIS, PROOF_QUALITY,
    VARIABLE_PITCH, "Bookman Old Style");

  VG4_RndPrimMatrConvert = MatrMulMatr(MatrMulMatr(MatrScale(0.0024, 0.0024, 0.0024), MatrRotateX(-0)), MatrTranslate(0, 0, 0));
  VG4_GeomLoad(&Uni->Helic, "MODELS\\Mi8\\havoc.g3d");
  Uni->Helic.ProgId = VG4_ShaderLoad("HELIC");


  VG4_RndPrimDefaultColor = ColorSet(1, 0, 0, 1);
  VG4_PrimCreatePlane(&pr, VecSet(-500, -0.030, 0), VecSet(1000, 0, 0), VecSet(0, 0.059, 0), 2, 2);
  VG4_GeomAddPrim(&Uni->Axes, &pr);

  VG4_RndPrimDefaultColor = ColorSet(0, 0, 1, 1);
  VG4_PrimCreatePlane(&pr, VecSet(0, -0.030, -500), VecSet(0, 0, 1000), VecSet(0, 0.059, 0), 2, 2);
  VG4_GeomAddPrim(&Uni->Axes, &pr);


  VG4_RndPrimDefaultColor = ColorSet(0, 1, 0, 1);
  VG4_PrimCreatePlane(&pr, VecSet(-0.030, -500, 0), VecSet(0, 1000, 0), VecSet(0.059, 0, 0), 2, 2);
  VG4_GeomAddPrim(&Uni->Axes, &pr);

  Uni->Axes.ProgId = VG4_ShaderLoad("AXIS");

  VG4_RndPrimDefaultColor = ColorSet(0.7, 0.5, 0.3, 1);
  VG4_PrimCreateSphere(&Uni->Sph, VecSet(0, 0, 0), 1, 8, 15);
  Uni->Sph.ProgId = Uni->Axes.ProgId;

  /* ��������� ��������� ��������� */
  Uni->Pos = VecSet(0, 18, 0);
  Uni->V = 0;
  Uni->Head = 0;
  Uni->Omega = 0;

  Uni->CPos = VecSet(59, 30, 59);
} /* End of 'VG4_AnimUnitInit' function */

/* ������� ��������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_CTRL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitClose( vg4UNIT_CTRL *Uni, vg4ANIM *Ani )
{
  DeleteObject(Uni->hFnt);
  VG4_ShaderFree(Uni->Helic.ProgId);
  VG4_GeomFree(&Uni->Helic);
} /* End of 'VG4_AnimUnitClose' function */

/* ������� ���������� ����������� ���������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_CTRL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitResponse( vg4UNIT_CTRL *Uni, vg4ANIM *Ani )
{
  VEC Dir, Move;
  static FLT y = -0.067452297, z = 2.6340842;
  static DBL time = 5;

  time += VG4_Anim.GlobalDeltaTime;
  if (time > 5)
  {
    time = 0;
    VG4_ShaderFree(Uni->Axes.ProgId);
    VG4_ShaderFree(Uni->Helic.ProgId);
    Uni->Helic.ProgId = VG4_ShaderLoad("HELIC");
    Uni->Axes.ProgId = VG4_ShaderLoad("AXIS");
    Uni->Sph.ProgId = Uni->Axes.ProgId;
  }

  /* ���������������� ��������� */
  Uni->Omega = -3000 * Ani->JR * Ani->DeltaTime;
  Uni->Head += -3 * 30 * Ani->JR * Ani->DeltaTime;

  Dir = VecMulMatr3(VecSet(0, 0, 1), MatrRotateY(Uni->Head));

  Uni->V += -30 * Ani->JY * Ani->DeltaTime;
  Uni->V *= max(1 - Ani->GlobalDeltaTime, 0);
  Uni->Pos = VecAddVec(Uni->Pos, VecMulNum(Dir, Uni->V * Ani->DeltaTime));

  Uni->Pos = VecAddVec(Uni->Pos, VecMulNum(VecMulMatr3(Dir, MatrRotateY(-90)), 30 * Ani->JX * Ani->DeltaTime));

  Uni->Pos.Y += 30 * (Ani->JButs[1] - Ani->JButs[2]) * Ani->DeltaTime;

  /* ����� "��������" */
  Uni->At = VecSubVec(Uni->Pos, VecMulNum(Dir, 9));
  Uni->At.Y += 5.30;

  Move = VecSubVec(Uni->At, Uni->CPos);
  Uni->CPos = VecAddVec(Uni->CPos, VecMulNum(Move, Ani->DeltaTime));



  VG4_RndMatrView = MatrView(Uni->CPos,
                             Uni->Pos,
                             VecSet(0, 1, 0));

  if (Ani->KeysClick['W'])
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  if (Ani->KeysClick['Q'])
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  if (Ani->Keys[VK_ESCAPE])
    VG4_AnimDoExit();
  if (Ani->KeysClick['F'])
    VG4_AnimFlipFullScreen();
  if (Ani->KeysClick['P'])
    VG4_AnimSetPause(!Ani->IsPause);

  /* �������� ��������� */
  Uni->Helic.Prims[2].M = MatrRotateY(999 * Ani->Time);
  Uni->Helic.Prims[4].M = MatrMulMatr(MatrMulMatr(MatrTranslate(0, y, z), MatrRotateX(888 * Ani->Time)), MatrTranslate(0, -y, -z));
  y += Ani->Keys[VK_ADD] * Ani->GlobalDeltaTime * 0.1 - Ani->Keys[VK_SUBTRACT] * Ani->GlobalDeltaTime * 0.1;
  z += Ani->Keys['A'] * Ani->GlobalDeltaTime * 0.1 - Ani->Keys['Z'] * Ani->GlobalDeltaTime * 0.1;
} /* End of 'VG4_AnimUnitResponse' function */

/* ������� ���������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       vg4UNIT_CTRL *Uni;
 *   - ��������� �� �������� ��������:
 *       vg4ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VG4_AnimUnitRender( vg4UNIT_CTRL *Uni, vg4ANIM *Ani )
{
  HFONT hFntOld = SelectObject(Ani->hDC, Uni->hFnt);
  static DBL count = 30;
  static CHAR Buf[1000];

  count += Ani->GlobalDeltaTime;
  if (count > 1)
  {
    count = 0;
    sprintf(Buf, "FPS: %.3f", Ani->FPS);
    SetWindowText(Ani->hWnd, Buf);
  }

  /* �������� */
  VG4_RndMatrWorld =
    MatrMulMatr(MatrMulMatr(MatrMulMatr(
      MatrRotateX(-Ani->JY * 30), MatrRotateZ(Ani->JX * 59)),
      MatrRotateY(Uni->Head)), MatrTranslate(Uni->Pos.X, Uni->Pos.Y, Uni->Pos.Z));
  VG4_GeomDraw(&Uni->Helic);

  /* ��� */
  VG4_RndMatrWorld = MatrIdentity();
  VG4_GeomDraw(&Uni->Axes);

  //VG4_RndMatrWorld = MatrTranslate(Uni->At.X, Uni->At.Y, Uni->At.Z);
  //VG4_PrimDraw(&Uni->Sph);

} /* End of 'VG4_AnimUnitRender' function */

/* ������� �������� ������� �������� "����������".
 * ���������: ���.
 * ������������ ��������:
 *   (vg4UNIT *) ��������� �� ��������� ������ ��������.
 */
vg4UNIT * VG4_UnitControlCreate( VOID )
{
  vg4UNIT_CTRL *Uni;

  if ((Uni = (VOID *)VG4_AnimUnitCreate(sizeof(vg4UNIT_CTRL))) == NULL)
    return NULL;
  /* ��������� ���� */
  Uni->Init = (VOID *)VG4_AnimUnitInit;
  Uni->Close = (VOID *)VG4_AnimUnitClose;
  Uni->Response = (VOID *)VG4_AnimUnitResponse;
  Uni->Render = (VOID *)VG4_AnimUnitRender;
  return (vg4UNIT *)Uni;
} /* End of 'VG4_UnitBallCreate' function */

/* END OF 'UCONTRL.C' FILE */
